package be.infofarm.distanceCalculations;

import be.infofarm.domain.Coordinate;
import be.infofarm.graphHopper.GraphHopperResponse;


import java.util.List;
import java.util.Map;

/**
 * @Author InfoFarm NV
 */
public class TravelMatrix {
    private static Map<OriginDestination, GraphHopperResponse> impedanceMatrix;

    public static void setImpedanceMatrix(Map<OriginDestination, GraphHopperResponse> matrix) {
        impedanceMatrix = matrix;
    }

    public static long getDistanceInMeters(String origin, String destination) {

        return impedanceMatrix.get(new OriginDestination(origin, destination)).getDistanceInMeters();
    }

    public static long getTimeInSeconds(String origin, String destination) {
        return impedanceMatrix.get(new OriginDestination(origin, destination)).getTimeInSeconds();
    }

    public static List<Coordinate> getRoute(String origin, String destination) {
        return impedanceMatrix.get(new OriginDestination(origin, destination)).getCoordinates();
    }
}
