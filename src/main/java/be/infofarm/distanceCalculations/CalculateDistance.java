package be.infofarm.distanceCalculations;

import be.infofarm.domain.Location;
import be.infofarm.graphHopper.GraphHopper;
import be.infofarm.graphHopper.GraphHopperResponse;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author InfoFarm NV
 */
public class CalculateDistance {
    public static Map<OriginDestination, GraphHopperResponse> getTravelMatrix(GraphHopper graphHopper,
                                                                              Collection<Location> locationList){
        Map<OriginDestination, GraphHopperResponse> travelMatrix = new HashMap<>();

        for (Location p1 : locationList) {
            for (Location p2 : locationList) {
                GraphHopperResponse response = graphHopper.sendRequest(p1.getCoordinate(), p2.getCoordinate()).get(0);

                travelMatrix.put(new OriginDestination(p1.getPlace(), p2.getPlace()), response);
            }
        }

        return travelMatrix;
    }
}
