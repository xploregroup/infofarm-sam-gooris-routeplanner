package be.infofarm.distanceCalculations;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author InfoFarm NV
 */
@AllArgsConstructor
@Getter
public class OriginDestination {
    private final String origin;
    private final String destination;

    @Override
    public String toString() {
        return "OriginDestination{" +
                "origin='" + origin + '\'' +
                ", destination='" + destination + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OriginDestination that = (OriginDestination) o;

        if (origin != null ? !origin.equals(that.origin) : that.origin != null) return false;
        return destination != null ? destination.equals(that.destination) : that.destination == null;
    }

    @Override
    public int hashCode() {
        int result = origin != null ? origin.hashCode() : 0;
        result = 31 * result + (destination != null ? destination.hashCode() : 0);
        return result;
    }
}
