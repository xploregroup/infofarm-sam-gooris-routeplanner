package be.infofarm.graphHopper;

import be.infofarm.domain.Coordinate;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

/**
 * Able to parse a response of GraphHopper
 *
 * @Author InfoFarm NV
 */
public class GraphHopperJsonParser {
	private static String PATHS = "paths";
	private static String DISTANCE = "distance";
	private static String TIME = "time";
	private static String POINTS = "points";
	private static String COORDINATES = "coordinates";
	
	public static List<GraphHopperResponse> parse(String json) {
		List<GraphHopperResponse> responses = new ArrayList<>();
		
		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(json).getAsJsonObject();
		
		for (JsonElement element : obj.getAsJsonArray(PATHS)) {
			int distanceInMeters = element.getAsJsonObject().getAsJsonPrimitive(DISTANCE).getAsInt();
			int timeInSeconds = element.getAsJsonObject().getAsJsonPrimitive(TIME).getAsInt()/1000;
			List<Coordinate> coordinates = createListOfCoordinates(element.getAsJsonObject().getAsJsonObject(POINTS));
			
			responses.add(new GraphHopperResponse(distanceInMeters, timeInSeconds, coordinates));
		}
		
		return responses;
	}
	
	private static List<Coordinate> createListOfCoordinates(JsonObject obj) {
		List<Coordinate> coordinates = new ArrayList<>();
		
		for (JsonElement element : obj.getAsJsonArray(COORDINATES)) {
			
			double lon = element.getAsJsonArray().get(0).getAsDouble();
			double lat = element.getAsJsonArray().get(1).getAsDouble();
			
			coordinates.add(new Coordinate(lon, lat));
		}
		
		return coordinates;
	}
}
