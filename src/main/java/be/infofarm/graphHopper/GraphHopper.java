package be.infofarm.graphHopper;

import be.infofarm.domain.Coordinate;
import be.infofarm.utils.WebUtils;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * Small wrapper class to communicate with the GraphHopper API
 *
 * @Author InfoFarm NV
 */
@AllArgsConstructor
public class GraphHopper {
	private final String url;
	private final int port;
	private final GraphHopperApiType apiType;
	private final GraphHopperTransportType apiTransportType;

	
	public List<GraphHopperResponse> sendRequest(Coordinate origin, Coordinate destination) {
		try {
			String url = createRequestUrl(origin, destination);
			return GraphHopperJsonParser.parse(WebUtils.getHTMLResponse(url));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private String createRequestUrl(Coordinate origin, Coordinate destination) {
		String url = getBaseUrl();
		url += createPoint(origin) + "&" + createPoint(destination) + "&";
		url += createTransportType() + "&" + "points_encoded=false";

		return url;
	}
	
	private String createTransportType() {
		return "vehicle=" + this.apiTransportType;
	}
	
	private String createPoint(Coordinate c) {
		return "point=" + c.getLat() + "," + c.getLon();
	}
	
	private String getBaseUrl() {
		return "http://" + this.url + ":" + this.port + "/" + this.apiType; 
	}
}
