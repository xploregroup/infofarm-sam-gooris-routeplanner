package be.infofarm.graphHopper;

import be.infofarm.domain.Coordinate;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * @Author InfoFarm NV
 */
@AllArgsConstructor
@Getter
public class GraphHopperResponse {
	private final long distanceInMeters;
	private final long timeInSeconds;
	private final List<Coordinate> coordinates;
}
