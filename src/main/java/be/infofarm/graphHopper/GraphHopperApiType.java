package be.infofarm.graphHopper;

import lombok.AllArgsConstructor;

/**
 * The different API possibilities.
 *
 * Currently only the router is supported
 *
 * @Author InfoFarm NV
 */
@AllArgsConstructor
public enum GraphHopperApiType {
    ROUTING("route?");

    private final String label;

    public static GraphHopperApiType fromLabel(String label) {
        for (GraphHopperApiType type : GraphHopperApiType.values()) {
            if (type.label.equals(label)) {
                return type;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return label;
    }
}