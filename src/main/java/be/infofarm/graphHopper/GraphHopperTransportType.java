package be.infofarm.graphHopper;

import lombok.AllArgsConstructor;

/**
 * Used transport mean for GraphHopper.
 *
 * Currently, only the car is supported
 *
 * @Author InfoFarm NV
 */
@AllArgsConstructor
public enum GraphHopperTransportType {
	CAR("car");

    private final String label;

    public static GraphHopperTransportType fromLabel(String label) {
        for (GraphHopperTransportType type : GraphHopperTransportType.values()) {
            if (type.label.equals(label)) {
                return type;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return label;
    }
}