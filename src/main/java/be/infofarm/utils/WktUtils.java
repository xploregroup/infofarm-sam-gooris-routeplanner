package be.infofarm.utils;

import be.infofarm.domain.Coordinate;

import java.util.Collection;


/**
 * @Author InfoFarm NV
 */
public class WktUtils {
	/**
	 * Will create a "WKT LINESTRING" from a list of coordinates
	 *
	 * @param coordinates representing a line string
	 *
	 * @return WKT LINESTRING of the given coordinates
	 */
	public static String createWktLineString(final Collection<Coordinate> coordinates) {
		String lineString = "LINESTRING(";
		for (Coordinate coordinate : coordinates) {
			lineString += coordinate.getLon() + " " + coordinate.getLat() + ",";
		}
		// Remove the last "," and close the WKT string
		lineString = lineString.substring(0,lineString.length() - 1) + ")";
		
		return lineString;
	}
}
