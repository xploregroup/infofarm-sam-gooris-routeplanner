package be.infofarm.domain;

import be.infofarm.distanceCalculations.TravelMatrix;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author InfoFarm NV
 */
@Getter
@AllArgsConstructor
public class Location {
    private final String place;
    private final String activity;
    private final Coordinate coordinate;

    /**
     * A measurement of distance (in this case distance in meters
     *
     * @param destination The destination
     *
     * @return The distance from the current place the the destination
     */
    public long getDistanceTo(Location destination) {
        long distance = TravelMatrix.getDistanceInMeters(place, destination.getPlace());

        return distance;
    }

    /**
     * The angle relative to the direction EAST.
     * @param location never null
     * @return in Cartesian coordinates
     */
    public double getAngle(Location location) {
        // Euclidean distance (Pythagorean theorem) - not correct when the surface is a sphere
        double latitudeDifference = location.getCoordinate().getLat() - coordinate.getLat();
        double longitudeDifference = location.getCoordinate().getLon() - coordinate.getLon();
        return Math.atan2(latitudeDifference, longitudeDifference);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location that = (Location) o;

        return place != null ? place.equals(that.place) : that.place == null;
    }

    @Override
    public int hashCode() {
        return place != null ? place.hashCode() : 0;
    }

    @Override
    public String toString() {
        return place;
    }
}
