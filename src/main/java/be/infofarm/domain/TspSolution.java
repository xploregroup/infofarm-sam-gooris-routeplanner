/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package be.infofarm.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.simplelong.SimpleLongScore;
import org.optaplanner.persistence.xstream.api.score.buildin.simplelong.SimpleLongScoreXStreamConverter;

import java.util.Collections;
import java.util.List;


/**
 * @Author InfoFarm NV
 */
@PlanningSolution
@Getter
@Setter
@NoArgsConstructor
@XStreamAlias("TspSolution")
public class TspSolution {
    /* List of locations */
    @ProblemFactCollectionProperty
    private List<Location> locationList;
    /* The start location of the TSP problem */
    @ProblemFactProperty
    private Domicile domicile;
    @PlanningEntityCollectionProperty
    @ValueRangeProvider(id = "visitRange")
    private List<Visit> visitList;
    @XStreamConverter(SimpleLongScoreXStreamConverter.class)
    @PlanningScore
    private SimpleLongScore score;

    public TspSolution(List<Location> locationList, Domicile domicile, List<Visit> visitList) {
        this.locationList = locationList;
        this.domicile = domicile;
        this.visitList = visitList;
    }

    @ValueRangeProvider(id = "domicileRange")
    public List<Domicile> getDomicileRange() {
        return Collections.singletonList(domicile);
    }

}
