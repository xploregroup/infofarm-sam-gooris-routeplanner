package be.infofarm.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;


/**
 * Class represents a coordinate
 *
 * @Author InfoFarm NV
 */
@AllArgsConstructor
@Getter
public class Coordinate implements Serializable {
	private static final long serialVersionUID = 1L;

	/** Longitude coordinate (x) */
	final private double lon;
	/** Latitude coordinate (y) */
	final private double lat;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Coordinate that = (Coordinate) o;

		if (Double.compare(that.lon, lon) != 0) return false;
		return Double.compare(that.lat, lat) == 0;
	}

	@Override
	public int hashCode() {
		int result;
		long temp;
		temp = Double.doubleToLongBits(lon);
		result = (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(lat);
		result = 31 * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
}
