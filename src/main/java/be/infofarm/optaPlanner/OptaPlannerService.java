package be.infofarm.optaPlanner;

import lombok.Getter;
import lombok.Setter;
import org.optaplanner.core.api.score.Score;
import org.optaplanner.core.api.score.constraint.ConstraintMatch;
import org.optaplanner.core.api.score.constraint.ConstraintMatchTotal;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.impl.score.director.ScoreDirector;
import org.optaplanner.core.impl.score.director.ScoreDirectorFactory;

import java.util.*;


/**
 * An OptaPlanner wrapper class
 *
 * @param <Solution_>
 * @Author InfoFarm NV
 */
@Getter
@Setter
public class OptaPlannerService<Solution_> {
    // --------------------------------------------------------------------------------------------------------
    // ------------------------------------------- Variables --------------------------------------------------
    // --------------------------------------------------------------------------------------------------------
    /**
     * Factory to create OptaPlanner related object
     */
    private final SolverFactory<Solution_> solverFactory;


    // --------------------------------------------------------------------------------------------------------
    // ---------------------------------------- Public functions ----------------------------------------------
    // --------------------------------------------------------------------------------------------------------
    /**
     * Solver configuration xml file. This xml file should only contain the basic settings
     * This configuration file cannot be changed. Hence if one wants to use another config file,
     * a new class should be created
     *
     * @param solverConfigFile Path to the config file
     */
    public OptaPlannerService(final String solverConfigFile) {
        this.solverFactory = SolverFactory.createFromXmlResource(solverConfigFile);
    }

    /**
     * Returns an overview of the broken constraints
     * It is a map with the constraint name (as written in the Drools file) as a key and a list of lists as value.
     *
     * The key refers to the constraint which is broken.
     * The value refers to the following:
     * The first list indicates the instances which caused this broken constraint (the size indicates how many times it was broken)
     * The second list (within the first list) indicates which objects were involved. If an object got the wrong material for instance, this list
     * wile have size 1. If the constraint is calculated based on more objects, all these objects will be included! The actual broken constraint might
     * be included as well
     *
     * If you use this function, you need to be aware that some kind of parsing is needed to be able to retrieve the correct information.
     *
     * @param solution The object from which the broken constraints should be extracted
     *
     * @return All information about the broken constraints as provided by OptaPlanner (but in a better format)
     */
    public Map<String, List<List<Object>>> getBrokenConstraints(final Solution_ solution) {
        final Map<String, List<List<Object>>> brokenConstraints = new HashMap<>();

        final ScoreDirector scoreDirector = getScoreDirector(solution);
        for (ConstraintMatchTotal cmt : (Collection<ConstraintMatchTotal>) scoreDirector.getConstraintMatchTotals()) {
            String constraintName = cmt.getConstraintName();
            for (ConstraintMatch constraintMatch : cmt.getConstraintMatchSet()) {
                List<List<Object>> listList = brokenConstraints.get(constraintName);
                if (listList == null) {
                    listList = new ArrayList<>();
                    brokenConstraints.put(constraintName, listList);
                }
                listList.add(constraintMatch.getJustificationList());
            }

        }

        return brokenConstraints;
    }


    /**
     * This function can be used to solve a given problem.
     *
     * @param toSolve planning problem to solve
     * @param planningDuration How long is the planner allowed to search for a solution?
     * @param secToStopIfNoBetterSolutionIsFound If no better solution is found within this time frame, the be.infofarm.solver will stop
     * @param stopScore Defines at which score the planner can stop searching (e.g. "0hard/0soft" for perfect score, "0hard/*soft" for no hard constraints broken)
     *
     * @return Solution for the planning problem
     */
    public Solution_ solvePlanningProblem(final Solution_ toSolve, final Long planningDuration,
                                          final Long secToStopIfNoBetterSolutionIsFound, final String stopScore) {
        final Solver<Solution_> planningSolver = buildSolver(planningDuration, secToStopIfNoBetterSolutionIsFound, stopScore);

        return planningSolver.solve(toSolve);
    }

    /**
     * Gets the score of the given solution
     *
     * @param solution solution from which we need to score
     *
     * @return Score of the solution
     */
    public Score getScoreOfSolution(final Solution_ solution) {
        final ScoreDirector scoreDirector = getScoreDirector(solution);
        scoreDirector.setWorkingSolution(solution);

        return scoreDirector.calculateScore();
    }


    // --------------------------------------------------------------------------------------------------------
    // --------------------------------------- Private functions ----------------------------------------------
    // --------------------------------------------------------------------------------------------------------
    /**
     * Creates a score director
     *
     * @param solution the solution that should be added to the score director
     * @return score director initialised with the given solution
     */
    private ScoreDirector getScoreDirector(final Solution_ solution) {
        final Solver<Solution_> planningSolver = buildSolver();
        final ScoreDirectorFactory scoreDirectorFactory = planningSolver.getScoreDirectorFactory();
        final ScoreDirector scoreDirector = scoreDirectorFactory.buildScoreDirector();
        scoreDirector.setWorkingSolution(solution);

        return scoreDirector;
    }


    private Solver<Solution_> buildSolver() {
        return solverFactory.buildSolver();
    }

    private Solver<Solution_> buildSolver(final Long planningDuration, final Long secToStopIfNoBetterSolutionIsFound,
                                          final String stopScore) {
        if (planningDuration != null) {
            this.solverFactory.getSolverConfig().getTerminationConfig().setSecondsSpentLimit(planningDuration);
        }
        if (secToStopIfNoBetterSolutionIsFound != null) {
            this.solverFactory.getSolverConfig().getTerminationConfig().setUnimprovedSecondsSpentLimit(secToStopIfNoBetterSolutionIsFound);
        }
        if (stopScore != null) {
            this.solverFactory.getSolverConfig().getTerminationConfig().setBestScoreLimit(stopScore);
        }

        return solverFactory.buildSolver();
    }
}