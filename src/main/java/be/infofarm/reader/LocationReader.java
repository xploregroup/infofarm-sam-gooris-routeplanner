package be.infofarm.reader;


import be.infofarm.domain.Coordinate;
import be.infofarm.domain.Location;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author InfoFarm NV
 */
public class LocationReader {
    public static List<Location> readLocations(File file) throws  IOException{
        final CSVParser csvParser = getCsvParser(',');
        final CSVReader csvReader = getCsvReader(file, csvParser);

        List<Location> placesAndActivities = new ArrayList<>();
        // Reading Records One by One in a String array
        String[] nextRecord;
        while ((nextRecord = csvReader.readNext()) != null) {
            String place = nextRecord[0];
            String activity = nextRecord[1];
            double lat = Double.parseDouble(nextRecord[2]);
            double lon = Double.parseDouble(nextRecord[3]);

            placesAndActivities.add(new Location(place, activity, new Coordinate(lon, lat)));
        }

        return placesAndActivities;
    }

    public static CSVParser getCsvParser(char separator) {
        final CSVParser csvParser =
                new CSVParserBuilder()
                        .withSeparator(separator)
                        .withIgnoreQuotations(true)
                        .build();
        return csvParser;
    }

    public static CSVReader getCsvReader(File file, CSVParser csvParser) throws IOException {
        final CSVReader csvReader =
                new CSVReaderBuilder(new BufferedReader(new FileReader(file)))
                        .withSkipLines(1)
                        .withCSVParser(csvParser)
                        .build();

        return csvReader;
    }
}
