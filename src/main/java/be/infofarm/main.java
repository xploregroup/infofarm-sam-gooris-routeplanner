package be.infofarm;



import be.infofarm.distanceCalculations.CalculateDistance;
import be.infofarm.distanceCalculations.TravelMatrix;
import be.infofarm.domain.Domicile;
import be.infofarm.domain.Location;
import be.infofarm.domain.TspSolution;
import be.infofarm.domain.Visit;
import be.infofarm.graphHopper.GraphHopper;
import be.infofarm.graphHopper.GraphHopperApiType;
import be.infofarm.graphHopper.GraphHopperTransportType;
import be.infofarm.optaPlanner.OptaPlannerService;
import be.infofarm.reader.LocationReader;
import be.infofarm.writer.OutputWriter;


import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;


/**
 * @Author InfoFarm NV
 */
public class main {
    public static void main(String[] args) throws IOException, URISyntaxException {
        Properties prop = getProperties();

        List<Location> locations = getLocations(prop);
        initialiseTravelMatrix(prop, locations);
        TspSolution tspInput = getTspInput(prop, locations);
        TspSolution tspSolution = getTspSolution(prop, tspInput);

        String locationOutputFile = prop.getProperty("locationOutputFile");
        String routeOutputFile = prop.getProperty("routeOutputFile");

        OutputWriter.writeQGisCompatibleFiles(locationOutputFile, routeOutputFile, tspSolution);
    }

    private static TspSolution getTspSolution(Properties prop, TspSolution tspInput) {
        String solverConfigFile = prop.getProperty("solverConfigFile");

        OptaPlannerService<TspSolution> planner  = new OptaPlannerService<>(solverConfigFile);
        long planningDuration = Long.parseLong(prop.getProperty("planningDurationInSec"));
        long secToStopIfNoBetterSolutionIsFound = Long.parseLong(prop.getProperty("secToStopIfNoBetterSolutionIsFound"));
        TspSolution tspSolution = planner.solvePlanningProblem(tspInput, planningDuration,
                                                            secToStopIfNoBetterSolutionIsFound, null);

        return tspSolution;
    }

    private static TspSolution getTspInput(Properties prop, List<Location> locations) {
        String tspStart = prop.getProperty("tspStart");
        Domicile domicile = null;
        List<Visit> visits = new ArrayList<>();
        long id = 0;
        for (Location location : locations) {
            if (location.getPlace().equals(tspStart)) {
                domicile = new Domicile(location);
            } else {
                Visit visit = new Visit(id++, location, null);
                visits.add(visit);
            }
        }

        // This makes sure, that we create a "chained sequence", something like
        // A -> B, B -> C, C -> D ...
        // First item is the domicile
        // Important! If you just randomly assign these variables, OptaPlanner will fail
        // to keep the chained variables together
        // -------------
        // Alternative is to use a construction heuristic
//        for (int i = 0; i < visits.size(); ++i) {
//            if (i == 0) {
//                visits.get(i).setPreviousStandstill(domicile);
//            } else {
//                visits.get(i).setPreviousStandstill(visits.get(i - 1));
//            }
//        }

        return new TspSolution(locations, domicile, visits);
    }

    private static Properties getProperties() {
        Properties prop = new Properties();

        try {
            ClassLoader classLoader = main.class.getClassLoader();
            FileInputStream ip = new FileInputStream(new File(classLoader.getResource("config.properties").toURI()));
            prop.load(ip);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return prop;
    }

    private static List<Location> getLocations(Properties prop) throws IOException, URISyntaxException {
        File locationFile = null;

        String pathToLocation = prop.getProperty("locationFileInResource");
        ClassLoader classLoader = main.class.getClassLoader();
        locationFile = new File(classLoader.getResource(pathToLocation).toURI());

        List<Location> locations =
                LocationReader.readLocations(locationFile);

        return locations;
    }

    private static void initialiseTravelMatrix(Properties prop, Collection<Location> locations) {
        GraphHopper graphHopper = getGraphHopper(prop);

        TravelMatrix.setImpedanceMatrix(CalculateDistance.getTravelMatrix(graphHopper, locations));
    }

    private static GraphHopper getGraphHopper(Properties prop) {
        String host = prop.getProperty("host");
        int port = Integer.parseInt(prop.getProperty("port"));

        GraphHopper graphHopper = new GraphHopper(host, port, GraphHopperApiType.ROUTING, GraphHopperTransportType.CAR);

        return graphHopper;
    }
}
