package be.infofarm.writer;

import be.infofarm.distanceCalculations.TravelMatrix;
import be.infofarm.domain.Location;
import be.infofarm.domain.TspSolution;
import be.infofarm.domain.Visit;
import be.infofarm.utils.WktUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @Author InfoFarm NV
 */
public class OutputWriter {

    public static void writeQGisCompatibleFiles(String locationFile, String routeFile, TspSolution solution) throws IOException {
        BufferedWriter locationWriter = new BufferedWriter(new FileWriter(locationFile));
        BufferedWriter routeWriter = new BufferedWriter(new FileWriter(routeFile));

        Map<Location, Location> odsMap = new HashMap<>();
        for (Visit visit : solution.getVisitList()) {
            odsMap.put(visit.getPreviousStandstill().getLocation(), visit.getLocation());
        }

        int counter = 0;
        List<Location> ods = new ArrayList<>();
        ods.add(solution.getDomicile().getLocation());
        Location dummy = odsMap.get(solution.getDomicile().getLocation());
        locationWriter.write("id,place,lat,lon\n");
        locationWriter.write(counter + "," + solution.getDomicile().getLocation().getPlace() + "," + solution.getDomicile().getLocation().getCoordinate().getLat() + "," + solution.getDomicile().getLocation().getCoordinate().getLon() + "\n");
        while (odsMap.get(dummy) != null) {
            locationWriter.write((++counter) + "," + dummy.getPlace() + "," + dummy.getCoordinate().getLat() + "," + dummy.getCoordinate().getLon() + "\n");
            ods.add(dummy);
            dummy = odsMap.get(dummy);
        }
        locationWriter.write((++counter) + "," + dummy.getPlace() + "," + dummy.getCoordinate().getLat() + "," + dummy.getCoordinate().getLon() + "\n");
        ods.add(dummy);
        ods.add(solution.getDomicile().getLocation());

        counter = 0;
        routeWriter.write("id,WKT\n");
        for (int i = 0; i < ods.size() - 1; ++i) {
            routeWriter.write((counter++) + ",\"" + WktUtils.createWktLineString(TravelMatrix.getRoute(ods.get(i).getPlace(), ods.get(i + 1).getPlace())) + "\"\n");
        }

        locationWriter.close();
        routeWriter.close();
    }
}
