# General
This small project was developed in response to this post (https://github.com/Forceflow/Ambiance_TSP/blob/master/README.md). Since this project and none of the other answers used OptaPlanner to solve the Traveling Salesman Problem (TSP), InfoFarm decided to develop a small piece of software to do so.

[OptaPlanner](https://www.optaplanner.org) is a open-source constraint solver engine developed in Java. It can be used for a a lot of use cases, including a TSP.

Furthermore, we used [GraphHopper](https://www.graphhopper.com) and [OpenStreetMap (OSM)](https://www.openstreetmap.org) to calculate real routes instead of the Euclidian distance.

# Usage of the software
The software is developed together with maven and can be easily imported in InteliJ IDE. As mentioned before, GraphHopper is used for the distance calculations. GraphHopper can be used with an external API, or it can be run locally. In our case, we used GraphHopper on our localhost.

## Properties
The application uses some input variables, which can be configured in the config.properties file. We will explain them below:

1. host: The host on which GrapHopper is located
2. port: The port on which GraphHopper is listening
3. locationFileInResource: The input file including place, activity, lat, lon (should be located in the resource folder!)
4. tspStart: The name (place in the location input file) from where the TSP should start and end
5. solverConfigFile: The OptaPlanner solver xml (should be located in the resource folder!)
6. planningDurationInSec: Specifies how long OptaPlanner is allowed to search for a solution
7. secToStopIfNoBetterSolutionIsFound: Specifies that time in seconds when OptaPlanner can stop if the score does not get better
8. locationOutputFile: location for the location output file
9. routeOutputFile: location for the routing output file

The output consists of two files. The first file contains the location in the order in which they are visited together with the coordinates and place name. The routing output file consists of WKT LINESTRING (= sequence of coordinates), representing the route. Both files can be used to visualize the solution. One can for example use [QGis](https://www.qgis.org/nl/site/)

# Solution
Our software found the following solution:

Mal -> Leut -> As -> Bree -> Peer -> Geel -> Schriek -> Haacht -> Leest -> Puurs -> Boom -> Niel -> Reet -> Duffel -> Lint -> Doel -> Sinaai -> Heist -> Gits -> Tielt -> Ghent -> Mere -> Lot -> Vorst -> Mal

In total, Sam should drive 638.331 meters in order to visit every location once, starting and ending in Mal.

![picture](imgs/tspSolution.pdf)
